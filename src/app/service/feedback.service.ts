import { Injectable } from '@angular/core';
import { Feedback } from './feedback';

@Injectable({
  providedIn: 'root'
})
export class FeedbackService {

  feedbacks= Array<Feedback>();

  constructor() { 
   // this.feedbacks.push( new Feedback('Sophie Westfahl', 'sophiewestfahl@web.de', 10, 'Thank you so much for all the lovely comments. I rate my customers with 10/10!!'))
  }
}
