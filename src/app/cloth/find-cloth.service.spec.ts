import { TestBed } from '@angular/core/testing';

import { FindClothService } from './find-cloth.service';

describe('FindClothService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FindClothService = TestBed.get(FindClothService);
    expect(service).toBeTruthy();
  });
});
