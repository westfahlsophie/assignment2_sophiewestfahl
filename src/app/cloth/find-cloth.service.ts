import { Injectable } from '@angular/core';
import { Cloth } from '../cloth/cloth';

@Injectable({
  providedIn: 'root'
})
export class FindClothService {

  items= Array<Cloth>();

  constructor() { 
    this.items.push(new Cloth ('../../assets/trouser.jpeg','Trouser', 'Some test content...', 24.99));
    this.items.push(new Cloth ('','Jacket', 'Some test content...', 34.99));
  }

}
