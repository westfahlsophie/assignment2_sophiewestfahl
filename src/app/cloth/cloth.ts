import { Url } from 'url';

export class Cloth {
    image: any;
    name: string;
    description: string;
    price: number;

    constructor(newImage: any, newName: string, newDescription: string, newPrice: number){ //
        this.image = newImage;
        this.name = newName;
        this.description = newDescription;
        this.price = newPrice;

    }
}
