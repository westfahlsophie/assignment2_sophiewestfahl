import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';

import{ Feedback } from '../service/feedback';
import{ FeedbackService } from '../service/feedback.service';

@Component({
  selector: 'app-create-comment',
  templateUrl: './create-comment.page.html',
  styleUrls: ['./create-comment.page.scss'],
})
export class CreateCommentPage implements OnInit {

  name: string;
  email: string;
  scale: number;
  comment: string;

  feedbacks= Array<Feedback>();

  constructor(private router: Router, private storage: Storage, public feedbackService: FeedbackService) { 
    this.feedbacks;
  }  

  ngOnInit() {
    this.scale = 5;

    this.storage.get('feedback').then((val) => {
      this.feedbacks = JSON.parse(val);
      if(!this.feedbacks){
        this.feedbacks = [];
      }
    });
  }

  pushComment(){
    this.feedbacks.push(new Feedback (this.name,this.email,this.scale,this.comment));

    this.storage.set('feedback',JSON.stringify(this.feedbacks));
    this.router.navigate(['../comments']);

    this.name = '';
    this.email= '';
    this.scale = 5;
    this.comment = '';
  }

  pushEvent(event){
    if(event.keyCode === 13){
      this.pushComment();
    }
  }

}


//html required 