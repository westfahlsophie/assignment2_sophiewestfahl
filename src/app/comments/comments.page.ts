import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CreateCommentPage } from "../create-comment/create-comment.page";
import { Feedback } from '../service/feedback';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-comments',
  templateUrl: './comments.page.html',
  styleUrls: ['./comments.page.scss'],
})
export class CommentsPage implements OnInit {

  feedbacks= Array<Feedback>();  

  constructor(private storage: Storage, private router: Router) { 
    this.storage.get('feedback').then((val) => {
      this.feedbacks = JSON.parse(val);      

      if(!this.feedbacks){
        this.feedbacks = [];
      }
    });

  } // private createCommentPage: CreateCommentPage

  ngOnInit() {

    this.storage.get('feedback').then((val) => {
      this.feedbacks = JSON.parse(val);      

      if(!this.feedbacks){
        this.feedbacks = [];
      }

    });

    console.log(this.feedbacks);
    //this.items = this.createCommentPage.feedbacks;
   // console.log("blabl" + this.items);
  }

  openCreateComment(){
    this.router.navigate(['../create-comment']);
  }

  clearStorage(){
    this.storage.clear();
  }

  
}
