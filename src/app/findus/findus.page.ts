import { Component, OnInit } from '@angular/core';
import { Map, tileLayer} from 'leaflet';
import L from 'leaflet';
import 'leaflet/dist/images/marker-shadow.png';
import 'leaflet/dist/images/marker-icon-2x.png';
import { LocationService } from '../location.service';


@Component({
  selector: 'app-findus',
  templateUrl: './findus.page.html',
  styleUrls: ['./findus.page.scss'],
})
export class FindusPage implements OnInit {
  
  map: Map;
  lat: number;
  lng: number;

  constructor(private locationService: LocationService) {}
  
  
  ngOnInit() {

    this.lat = 65;
    this.lng = 25;

    this.locationService.getLocation().then(data => {
      this.lat = data.coords.latitude;
      this.lng = data.coords.longitude;

    });

    this.loadMap();
  }

  loadMap(){
    const latVal = this.lat;
    const lngVal = this.lng;

    setTimeout(() => {
      this.map = new Map('map').setView([latVal, lngVal], 8);
      tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',{
        attribution: '',
        maxZoom: 18
      }).addTo(this.map);

      this.map.locate({
      }).on('locationfound', (e) =>{
        const markerGroup = L.featureGroup();
        const marker: any = L.marker([e.latitude, e.longitude]).on('click', () =>{
          alert('Marker clicked');
        });
        markerGroup.addLayer(marker);
        this.map.addLayer(markerGroup);
      })
    }, 50);
  }

  
}
