import { Component, OnInit } from '@angular/core';
import { Cloth } from '../cloth/cloth';
import { FindClothService } from '../cloth/find-cloth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-page1',
  templateUrl: './page1.page.html',
  styleUrls: ['../folder/folder.page.scss'],
})
export class Page1Page implements OnInit {

  items = Array<Cloth>();

  constructor( 
    private findClothService: FindClothService,
    private router: Router) {}

  ngOnInit() {
    this.items = this.findClothService.items;
  }

  openDetails(item){
    this.findClothService.items = item;
    this.router.navigate(['../details']);
  }

}
