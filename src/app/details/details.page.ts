import { Component, OnInit } from '@angular/core';
import { FindClothService } from '../cloth/find-cloth.service';
import { Cloth } from '../cloth/cloth';

@Component({
  selector: 'app-details',
  templateUrl: './details.page.html',
  styleUrls: ['./details.page.scss'],
})
export class DetailsPage implements OnInit {

  items = Array<Cloth>();

  constructor( private findClothService: FindClothService) { }

  ngOnInit() {
    this.items = this.findClothService.items;

  }

}
